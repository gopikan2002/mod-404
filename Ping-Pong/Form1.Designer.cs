﻿namespace Ping_Pong
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GameB = new System.Windows.Forms.Panel();
            this.Ball = new System.Windows.Forms.PictureBox();
            this.Start_button = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.GameB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.Ball)).BeginInit();
            this.SuspendLayout();
            // 
            // GameB
            // 
            this.GameB.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("GameB.BackgroundImage")));
            this.GameB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GameB.Controls.Add(this.Ball);
            this.GameB.Location = new System.Drawing.Point(62, 68);
            this.GameB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GameB.Name = "GameB";
            this.GameB.Size = new System.Drawing.Size(773, 416);
            this.GameB.TabIndex = 0;
            //this.GameB.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Ball
            // 
            this.Ball.BackColor = System.Drawing.Color.Blue;
            this.Ball.Location = new System.Drawing.Point(127, 139);
            this.Ball.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Ball.Name = "Ball";
            this.Ball.Size = new System.Drawing.Size(24, 25);
            this.Ball.TabIndex = 0;
            this.Ball.TabStop = false;
            // 
            // Start_button
            // 
            this.Start_button.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("Start_button.BackgroundImage")));
            this.Start_button.ForeColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))),
                ((int) (((byte) (192)))), ((int) (((byte) (192)))));
            this.Start_button.Location = new System.Drawing.Point(62, 489);
            this.Start_button.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Start_button.Name = "Start_button";
            this.Start_button.Size = new System.Drawing.Size(137, 55);
            this.Start_button.TabIndex = 1;
            this.Start_button.Text = "Start Game";
            this.Start_button.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 120;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 634);
            this.Controls.Add(this.Start_button);
            this.Controls.Add(this.GameB);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Ping_Pong";
            this.GameB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.Ball)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel GameB;
        private System.Windows.Forms.PictureBox Ball;
        private System.Windows.Forms.Button Start_button;
        private System.Windows.Forms.Timer timer1;
    }
}